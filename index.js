var express = require('express');
var bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
var app = express();
//console.log('coming here');
app.use('/css', express.static('css')
);
app.use('/vendors', express.static('vendors')
);
app.use('/fonts', express.static('fonts')
);

app.use('/img', express.static('img')
);
app.use('/js', express.static('js')
);
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');

});
app.get('/about-us', function (req, res) {
    res.sendFile(__dirname + '/about-us.html');

});
app.get('/contact', function (req, res) {
    res.sendFile(__dirname + '/contact.html');

});

app.listen(process.env.PORT ||3000);